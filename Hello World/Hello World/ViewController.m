//
//  ViewController.m
//  Hello World
//
//  Created by Mariuxi Yagual on 1/24/17.
//  Copyright © 2017 Mariuxi Yagual. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)didTouchUp:(id)sender{
    NSLog(@"Button Pressed!");
    UIColor *color1 = self.view.backgroundColor;
    UIColor *color2 = [UIColor greenColor];
    if ([color1 isEqual:color2]){
        self.view.backgroundColor = [UIColor redColor];
    }else{
        self.view.backgroundColor = color2;
    }
    self.lb.text = @"Mariuxi Yagual";
    
}

@end
