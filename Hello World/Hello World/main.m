//
//  main.m
//  Hello World
//
//  Created by Mariuxi Yagual on 1/24/17.
//  Copyright © 2017 Mariuxi Yagual. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
