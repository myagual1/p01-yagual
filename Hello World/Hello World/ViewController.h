//
//  ViewController.h
//  Hello World
//
//  Created by Mariuxi Yagual on 1/24/17.
//  Copyright © 2017 Mariuxi Yagual. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *butn;
@property (weak, nonatomic) IBOutlet UILabel *lb;

- (IBAction)didTouchUp:(id)sender;



@end

