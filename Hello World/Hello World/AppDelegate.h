//
//  AppDelegate.h
//  Hello World
//
//  Created by Mariuxi Yagual on 1/24/17.
//  Copyright © 2017 Mariuxi Yagual. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

